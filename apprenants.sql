-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jan 12, 2019 at 06:42 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `apprenants`
--

-- --------------------------------------------------------

--
-- Table structure for table `apprenants`
--

CREATE TABLE `apprenants` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `age` int(2) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `citation` varchar(255) NOT NULL,
  `auteur` varchar(50) NOT NULL,
  `genre` varchar(50) NOT NULL,
  `distance` varchar(10) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image_objet` varchar(255) NOT NULL,
  `carte` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `apprenants`
--

INSERT INTO `apprenants` (`id`, `nom`, `prenom`, `age`, `telephone`, `adresse`, `ville`, `citation`, `auteur`, `genre`, `distance`, `image`, `image_objet`, `carte`) VALUES
(1, 'AHARBIL', 'Zoubaïr', 26, '0786078922', 'La Rouairie', 'Le Bourgneuf-la-Forêt', 'Exige beaucoup de toi-même et attends peu des autres. Ainsi beaucoup d’ennuis te seront épargnés.', 'Confucius', 'masculin', '5km', 'images/apprenants/DSC_7934.jpg', 'images/apprenants/DSC_7934-alt.jpg', 'https://goo.gl/maps/zkfnKJuYYSC2'),
(2, 'BAYLAC', 'Julien', 28, '0678679913', '11 square des hautes ourmes', 'Rennes', 'C’est ce que nous sommes tous, des amateurs, on ne vit jamais assez longtemps pour être autre chose.\r\n', 'Charlie Chaplin', 'masculin', '5km', 'images/apprenants/DSC_7983.jpg', 'images/apprenants/DSC_7983-alt.jpg', 'https://goo.gl/maps/td74BJkQiuk'),
(3, 'BRUNETIERE', 'Delphine', 27, '0771858690', '', 'Rennes', 'You’ll never find rainbows\r\nIf you’re looking down\r\n', 'Charlie Chaplin', 'feminin', '10km', 'images/apprenants/DSC_7979.jpg', 'images/apprenants/DSC_7979-alt.jpg', 'https://goo.gl/maps/NUPPHSyXG4k'),
(4, 'CARRY', 'Alice', 25, '0674334850', '18 rue du Puits Jacob', 'Rennes', '', '', 'feminin', '5-10km', 'images/apprenants/DSC_7962.jpg', 'images/apprenants/DSC_7962-alt.jpg', 'https://goo.gl/maps/M2sohGsjy4J2'),
(5, 'CHEVALIER', 'Honorin', 23, '0657984130', '', 'Rennes', 'Ne pas confondre culture & intelligence', 'Un chien', 'masculin', '5-10km', 'images/apprenants/DSC_7930.jpg', 'images/apprenants/DSC_7930-alt.jpg', 'https://goo.gl/maps/y81YNUD68YE2'),
(6, 'DROUMAGUET', 'Ann-Katell', 49, '0679603205', '7 avenue du Gros Malhon', 'Rennes', 'Tout va pour le mieux dans le meilleur des mondes', 'Voltaire (Candide)', 'feminin', '10km', 'images/apprenants/DSC_7971.jpg', 'images/apprenants/DSC_7971-alt.jpg', 'https://goo.gl/maps/xBVVZy2M5j22'),
(7, 'GONTHIER', 'Audrey', 28, '0760433784', '', 'Cesson-Sévigné', 'Vous aviez à choisir entre le déshonneur et la guerre. Vous avez choisi le déshonneur, vous aurez la guerre.', 'Winston Churchill', 'feminin', '5-10km', 'images/apprenants/DSC_7944.jpg', 'images/apprenants/DSC_7944-alt.jpg', 'https://goo.gl/maps/qySKQ7inLAu'),
(8, 'HENNEBERT', 'Nora', 19, '0668130757', '6 rue du Bobéril', 'Rennes', 'Carpe Diem', 'Horace', 'feminin', '5-10km', 'images/apprenants/DSC_7966.jpg', 'images/apprenants/DSC_7966-alt.jpg', 'https://goo.gl/maps/dEuh2PoASEG2'),
(9, 'JUHEL', 'Quentin', 25, '0612567893', '42 La Ville Goudelin', 'Dinan', 'Je n’ai pas à gagner ma vie, je l’ai', 'Boris Vian', 'masculin', '10km', 'images/apprenants/DSC_7932.jpg', 'images/apprenants/DSC_7932-alt.jpg', 'https://goo.gl/maps/wKTvJByfchF2'),
(10, 'LARQUETOUX', 'Carol', 53, '0676498870', '23 square Louis Boulanger', 'Rennes', 'La chance sourit aux audacieux', 'Virgile', 'feminin', '5-10km', 'images/apprenants/DSC_7985.jpg', 'images/apprenants/DSC_7985-alt.jpg', 'https://goo.gl/maps/SFmYUjjh5RT2'),
(11, 'LENORMAND', 'Emmanuelle', 23, '0620576596', '5 Place du Landrel', 'Rennes', '', '', 'feminin', '5-10km', 'images/apprenants/DSC_7960.jpg', 'images/apprenants/DSC_7960-alt.jpg', 'https://goo.gl/maps/LJYGbzUnLWp'),
(12, 'MPOY', 'Jean-Marc', 25, '0664418544', '22 Square Professeur Louis Antoine', 'Rennes', 'There’s nobody that talks to you more in a day than yourself.', 'Elena Delle Donne', 'masculin', '5km', 'images/apprenants/DSC_7976.jpg', 'images/apprenants/DSC_7976-alt.jpg', 'https://goo.gl/maps/QYNuBHwVUDL2'),
(13, 'NAGOU', 'Maëva', 28, '0610488352', '81 rue Adolphe Leray', 'Rennes', 'La vie, ce n’est pas d’attendre que les orages passent, c’est d’apprendre à danser sous la pluie.', 'Musso', 'feminin', '10km', 'images/apprenants/DSC_7974.jpg', 'images/apprenants/DSC_7974-alt.jpg', 'https://goo.gl/maps/4KS9udSvf3E2'),
(14, 'NICOLO', 'Régis', 33, '0699345818', '9 rue de Rennes', 'Saint-Aubin d’Aubigné', 'Il n’y a pas de grande tâche difficile qui ne puisse être décomposée en petites tâches faciles', 'Matthieu Ricard', 'masculin', '5km', 'images/apprenants/DSC_7968.jpg', 'images/apprenants/DSC_7968-alt.jpg', 'https://goo.gl/maps/CJJUPM9FRWr'),
(15, 'PELTIER', 'Martin', 24, '0685845360', '137 Avenue Aristide Briand', 'Rennes', 'Whatever you do, don\'t be another brick in the wall', 'Moody, Californication', 'masculin', '5-10km', 'images/apprenants/DSC_7948.jpg', 'images/apprenants/DSC_7948-alt.jpg', 'https://goo.gl/maps/mH2Pyo3mx632'),
(16, 'QUENTEL', 'Philippe', 28, '0663681213', '1 Rue Porcon de la Barbinais', 'Rennes', 'Les hommes intelligents ne peuvent être de bons maris, pour la bonne raison qu’ils ne se marient pas', 'Henry de Montherlant', 'masculin', '10km', 'images/apprenants/DSC_7953.jpg', 'images/apprenants/DSC_7953-alt.jpg', 'https://goo.gl/maps/N5kh56i4PqR2'),
(17, 'RIVIERE', 'Ewilan', 26, '0673154725', '14 La Boutonnerie', 'Liffré', 'Celui qui croit savoir n’apprend plus', 'P. Bottero', 'feminin', '5-10km', 'images/apprenants/DSC_7946.jpg', 'images/apprenants/DSC_7946-alt.jpg', 'https://goo.gl/maps/XBes2fX87yF2'),
(18, 'SAVE', 'Gaëtan', 21, '0688581126', 'Les Champs Géraux', 'Rennes', 'Le devoir a une grande ressemblance avec le bonheur d’autrui', 'Victor Hugo', 'masculin', '5-10km', 'images/apprenants/DSC_7958.jpg', 'images/apprenants/DSC_7958-alt.jpg', 'https://goo.gl/maps/bMRWYvn6GRm'),
(19, 'SHAFAQ-ZADAH', 'Maysam', 20, '0606852779', '21 boulevard Charles Péguy\r\n', 'Rennes', 'Follow your bliss and the universe will open doors where there were only walls', 'Joseph Campbell', 'masculin', '10km', 'images/apprenants/DSC_7940.jpg', 'images/apprenants/DSC_7940-alt.jpg', 'https://goo.gl/maps/Q8RQFntTtbK2'),
(20, 'SHAFAQ-ZADAH', 'Meuslem', 20, '0674726560', '21 boulevard Charles Peguy', 'Rennes', 'Do not let your difficulties fill you with anxiety, after all it is only in the darkest nights that stars shine more brightly', 'Ali Ibn Abi Taleb', 'masculin', '10km', 'images/apprenants/DSC_7940.jpg', 'images/apprenants/DSC_7940-alt.jpg', 'https://goo.gl/maps/Q8RQFntTtbK2'),
(21, 'SIMONETTI', 'Lionel', 36, '0663259718', '7 bd louis Volclair\r\n', 'Rennes', 'Tout obstacle renforce la détermination. Celui qui s’est fixé un but n’en change pas\r\n', 'Léonard De Vinci', 'masculin', '5km', 'images/apprenants/DSC_7950.jpg', 'images/apprenants/DSC_7950-alt.jpg', 'https://goo.gl/maps/qZEJuAYo5xr'),
(22, 'TAS', 'Joanna', 39, '0631472229', '2 avenue du Canada', 'Rennes', '', '', 'feminin', '5km', 'images/apprenants/DSC_7981.jpg', 'images/apprenants/DSC_7981-alt.jpg', 'https://goo.gl/maps/sQysEBJdPBs'),
(23, 'SCHINDELMAN', 'Hugo', 25, '0663182210', '2 rue de la Vigne', 'Le Rheu', '', '', 'masculin', '5-10km', './assets/images/DSC_7958.jpg', './assets/images/DSC_7958-alt.jpg', 'https://goo.gl/maps/GAfNkkXHvM22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apprenants`
--
ALTER TABLE `apprenants`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apprenants`
--
ALTER TABLE `apprenants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
