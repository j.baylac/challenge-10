
<!-- generer le head en html -->
<?php
function generer_head() {

  echo "<!doctype html>";
  echo "<html lang='fr'>";

  echo "<head>";
  echo "<meta charset='utf-8'>";

  echo "<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>";

  echo "<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css' integrity='sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO' crossorigin='anonymous'>";

  echo "<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.4.2/css/all.css' integrity='sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns' crossorigin='anonymous'>";

  echo "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>";
  echo "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>";

  echo "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>";

  echo "<title>[Code Académie] Promo #3 - Trombinoscope</title>";
  echo "</head>";

    echo "<body>";
}
?>

  <!-- generer le body en html -->
  <?php
  function generer_body() {


  echo "<div class='modal fade' id='exampleModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>";
            echo "<div class='modal-dialog' role='document'>";
              echo "<div class='modal-content'>";
                echo "<div class='modal-header'>";
                  echo "<h5 class='modal-title font-weight-bold' id='StudentsNames'></h5>";

                  echo "<button type='button' class='close' data-dismiss='modal' aria-label='Close'>";
                    echo "<span aria-hidden='true'>&times;</span>";
                  echo "</button>";
                echo "</div>";

                echo "<div class='modal-body'>";
                   echo "<div class='row'>";
                      echo "<div class='col-sm-6'>";

                      echo "<img class='card-img-top m-0' id='StudentsImages' src='' />";
                          echo "<div class='mt-4'>";
                              echo "<span class='badge badge-pill badge-dark'><a id='StudentsGender'></a></span>";
                              echo "<span class='badge badge-pill badge-dark'><a id='StudentsDistance'></a></span>";
                          echo "</div>";
                      echo "</div>";

                      echo "<div class='col-sm-6'>";
                                  echo "<p class='font-weight-bold'>Age : <a class='font-weight-normal' id='StudentsAges'><a><a class='font-weight-normal'> ans</a></p>";
                                  echo "<p class='font-weight-bold'>Téléphone : <a class='font-weight-normal' id='StudentsPhones'></a></p>";
                                  echo "<p class='font-weight-bold' >Adresse :<br/>";
                                  echo "<div class='d-flex justify-content-between'>";
                                  echo "<div>";
                                      echo "<a class='font-weight-normal' id='StudentsAdresses'></a><br/>";
                                      echo "<a class='font-weight-normal' id='StudentsCities'></a></p>";
                                  echo "</div>";
                                  echo "<div>";
                                      echo "<a id='StudentsMaps' href='' style='font-size:11px' class='btn btn-secondary btn-lg ml-5' role='button' aria-disabled='true'><i class='fas fa-map-marker-alt'></i></a>";
                                  echo "</div>";
                                  echo "</div>";
                                  echo "<p class='font-weight-bold'>Citation préférée :</p>";
                                  echo "<a class='font-weight-normal' id='StudentsCitations'></a><br/>";
                                  echo "<p class='font-weight-light font-italic'>- <a id='StudentsCitationsWitters'></a></p>";
                      echo "</div>";
                  echo "</div>";
              echo "</div>";

                    echo "<div class='modal-footer'>";
                  echo "<button type='button' class='btn btn-info' data-dismiss='modal'>Fermer</button>";
                echo "</div>";
              echo "</div>";
            echo "</div>";
          echo "</div>";

      echo "<div class='container'>";

      echo "<header class='row bg-white'>";
          echo "<div class='col-lg-12 p-0 align-middle'>";
              echo "<a href='index.html'><img src='./assets/images/logo-codeacademie.png' alt='logo code academie' class='float-left col-1 p-0 mt-2'></a>";
              echo "<span class='d-flex justify-content-between'>";
                echo "<h4 class='mt-4 ml-4 font-weight-bold'>Promo #3</h4>";
                echo "<pre class='mt-3'><code class='m-0'>&lt;limite&gt;&lt;/limit&gt;</code></pre>";
              echo "</span>";
          echo "</div>";
      echo "</header>";
              echo "<div class='row'>";
                  echo "<div class='col-2 bg-dark'>";
                      echo "<div class='row'>";
                          echo "<div class='col-12 p-3 d-flex align-items-center justify-content-between'>";
                              echo "<h4 class='text-light mb-0'>Filtres</h4>";
                              echo "<a href='#' id='btn-reset' class='btn btn-sm btn-light'><i class='fas fa-undo'></i></a>";
                          echo "</div>";
                      echo "</div>";
                      echo "<p class='text-light border-top pt-2 mb-1'>Genre :</p>";
                      echo "<a href='#' id='btn-toggle-masculin' class='btn btn-light btn-sm'>Masculin</a>";
                      echo "<a href='#' id='btn-toggle-feminin' class='btn btn-light btn-sm'>Féminin</a>";
                  echo "</div>";
                  echo "<div class='col-10'>";

                      echo "<div class='row'>";

                          echo "<div class='col-12 bg-info text-light p-3'>";
                              echo "<h4 class='m-0'>Nombre d'apprenants concernés : <span id='studentsConcernedNumber' class='font-weight-bold'></span></h4>";
                          echo "</div>";
                      echo "</div>";
                      echo "<div id='students' class='row py-3'>";
                    }
                  ?>


                  <!-- generer le trombi en html -->
                  <?php
                  function generer_trombi() {
                    for ($i = 1; $i <= 23; $i++)
                    {
                    $id = $i;
                    // Création du DSN
                    $dsn = 'mysql:host=localhost; dbname=apprenants; port=8888; charset=utf8';
                    try {
                    $pdo = new PDO($dsn, 'root' , 'root');
                    }
                    catch (PDOException $exception) {
                     mail('julien.baylac@protonmail.com', 'PDOException', $exception->getMessage());
                     exit('Erreur de connexion à la base de données');
                    }
                    $query = $pdo->query("SELECT * FROM `apprenants` WHERE id = ".$id."");

                    $resultat = $query->fetchAll();
                    foreach ($resultat as $key => $variable)
                    {
                          echo "<div class='col-3 mb-3' data-toggle='modal' data-target=''#exampleModal' onclick='infos(0);''>";
                              echo "<div class='card border border-white rounded-0 pb-3 h-100 ".$resultat[$key]['genre']."'>";
                                  echo "<img class='card-img-top rounded-0 mh-100' src='./assets/".$resultat[$key]['image']."' />";
                                  echo "<div class='card-body p-2'>";
                                      echo "<h5 class='card-title m-0'>".$resultat[$key]['prenom']." ".$resultat[$key]['nom']."</h5>";
                                      echo "<span class='badge badge-pill badge-dark'>#".$resultat[$key]['genre']."</span>";
                                  echo "</div>";
                              echo "</div>";
                          echo "</div>";
                     }
                    }
                   }
                  ?>

                  <!-- generer la fin du html  -->
                  <?php
                  function generer_fin() {
                      echo "</div>";
                  echo "</div>";
              echo "</div>";
          echo "</div>";
          echo "<script type='text/javascript' src='assets/js/script.js'></script>";
      echo "</body>";
  echo "</html>";
}
?>
