<!DOCTYPE html>
<html lang="fr">

    <head>

        <meta charset="utf-8">
        <title>[Code Académie] Promo #3</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Connexion à mon application">
        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" />
        <!-- ci-dessous notre fichier CSS -->
        <link rel="stylesheet" type="text/css" href="/css/app.css" />
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato:400,700,300" />
        <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    </head>
    <body>

<script>
  body {
  background: #252525;
  font-family: 'Open Sans';
  font-weight: 300;
}
.main {
  background: white url(machine_a_ecrire.jpg) right top no-repeat;
  background-size: contain;
  padding: 80px 20px 20px;
  margin-top: 120px;
}
@media only screen and (max-width : 992px) {
  .main {
      background: white;
      margin-top: 30px;
  }
}
h1 {
  font-family: 'Lato', sans-serif;
  font-weight: 300;
  color: #555;
  margin-bottom: 0;
}
h2 {
  font-family: 'Lato', sans-serif;
  font-weight: 300;
  color: #999;
  font-size: 18px;
  margin-top: 5px;
}
form {
  margin-top: 60px;
}
input.form-control , input[type="submit"] , .btn {
  border-radius: 0px;
}
.btn {
  transition: all ease-in-out 0.2s;
}
.credits {
  margin-top: 100px;
  color: #999;
  font-size: 12px;
}
.credits a {
  color: inherit;
}
</script>

      <div class="container">
      <div class="row">
      <div class="col-xs-12">

          <div class="main">

              <div class="row">
              <div class="col-xs-12 col-sm-6 col-sm-offset-1">

                  <h1>[Code Académie] Promo #3</h1>
                  <h2>Trombinoscope</h2>

                  <form action="/users/login" name="login" role="form" class="form-horizontal" method="post" accept-charset="utf-8">
                      <div class="form-group">
                      <div class="col-md-8"><input name="username" placeholder="Idenfiant" class="form-control" type="text" id="UserUsername"/></div>
                      </div>

                      <div class="form-group">
                      <div class="col-md-8"><input name="password" placeholder="Mot de passe" class="form-control" type="password" id="UserPassword"/></div>
                      </div>

                      <div class="form-group">
                      <div class="col-md-offset-0 col-md-8"><input  class="btn btn-success btn btn-success" type="submit" value="Connexion"/></div>
                      </div>

                  </form>
              </div>
              </div>

          </div>
      </div>
      </div>
      </div>
    </body>
</html>
